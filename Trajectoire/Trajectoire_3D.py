from math import *
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numpy import *
import numpy as np

#Ce programme affiche la trajectoire d'un point du tore en lui appliquant la fonction phi

#Ce programme va tout d'abord chercher la trajectoire en 2D puis appliquer la fonction d'un tore pour afficher les positions en 3D

def trajectoire_3d(a, b, n):    # n : nombre de points sur un cercle du tore; a, b : position de depart
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x0=a
    y0=b
    Lx=[x0]     #Listes des positions  
    Ly=[y0]
    xk=(x0+y0)%n    #On applique la fonction phi
    yk=(x0+2*y0)%n
    while (xk != x0) or (yk != y0): #Tant qu'on ne revient pas a la position de debut
        Lx.append(xk)       #On ajoute la nouvelle position aux listes
        Ly.append(yk)
        xk, yk=(xk+yk)%n, (xk+2*yk)%n   #on applique phi
    theta = [2*pi*k/n for k in Lx]                              #
    phi = [2*pi*k/n for k in Ly]                                #
    X = np.outer((1 + 0.2 * np.cos(phi)), np.cos(theta))        #On affiche les position en 3D
    Y = np.outer((1 + 0.2 * np.cos(phi)), np.sin(theta))        #
    Z = np.outer(np.sin(phi), 0.2 * np.ones(np.size(theta)))    #
    ax.scatter(X, Y, Z)
    ax.set_zlim3d(-0.45,0.45)
    plt.show()

trajectoire_3d(5, 4, 20)


   
