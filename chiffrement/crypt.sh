#!/bin/bash
echo "Exemple:	  crypt.sh --img --lock 5 3 2 monImage.jpg"
#		     0	      1     2   3 4 5     6

if [ $# -lt 6 ]
then
   echo "Vous n'avez que $# arguments: il en faut 6"
   exit
fi


# Verification de l'existance du fichier source
if [ ! -e $6 ]
then
   echo "Le fichier $6 n'existe pas"
   exit
fi

# Choix du module (Texte ou Image)
case $1 in
   "--txt") cmd="chiffrer_texte.py"; echo $cmd;;
   "--img") cmd="chiffrer_image.py"; echo $cmd;;
   *) echo "Module '$1' inconnu. Utilisez '--txt' ou '--img'"; exit;;
esac

# Verification de l'action (lock ou unlock)
case $2 in
   "--lock") echo "Chiffrement du fichier";;
   "--unlock") echo "Dechiffrement du fichier";;
   *) echo "Action '$2' inconnue. Utilisez '--lock' ou '--unlock'"; exit;;
esac

	    # --txt
	    # --img
# python3   chiffrer_texte.py monImage.jpg --lock 5 3 2 


# Appel de la commande
python3 $cmd $6 $2 $3 $4 $5

