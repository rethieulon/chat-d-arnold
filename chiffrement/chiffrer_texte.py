from numpy import *
import numpy as np
import sys

#Ce programme permet de chiffrer et déchiffrer un texte à l'aide des matrices de transformation du chat d'Arnold

#Soit phi: [0,n-1]*[0,n-1] -> [0,n-1]*[0,n-1]
#               (x, y)     -> phi(x,y)=((1,a),(b,1+a*b)).(x,y)


#Fonction phi sur une matrice avec les paramètre a et b
def phi(mat_orig, a, b):
    mat = np.copy(mat_orig)   #On copie la matrice originale pour la modifier
    (n, p) = mat.shape
    for i in range (0, n):
        for j in range (0, n):
            mat[i,j] = mat_orig[(i+j*a)%n, (i*b+j*(1+a*b))%n] #Fonction phi sur chaque élement de la matrice
    return mat


#Fonction de chiffrement du texte
def chiffre(fichier_lecture, fichier_ecriture, a, b, n):
    texte_lire = open(fichier_lecture, 'r')
    lect = texte_lire.read()                 #On met le texte dans une chaine de caractères
    longueur = len(lect)
    taille = int(sqrt(longueur))+1
    A = zeros((taille, taille), dtype=str)   #On créé une matrice de caractères
    A[:] = '0'
    i = taille-1
    j = 0
    for k in range (0,len(lect)):            #Dans chaque case de la matrice, on y met un caractère de la chaine
        A[i,j] = lect[k]
        j+=1
        if j == taille:
            j = 0
            i-=1
    for k in range (0, n):
        A = phi(A, a, b)                     #On applique n fois la fonction phi sur la matrice
    ecrit=""
    for i in range (0, taille):
        for j in range (0, taille):
            ecrit = ecrit + A[i,j]           #On écrit les éléments de la matrice à la suite dans une chaine des caractères
    texte_ecrire= open(fichier_ecriture, 'w')
    texte_ecrire.write(ecrit)
    texte_ecrire.close()
    texte_lire.close()



# chiffre("texte.txt", "texte_chiffre.txt", 5, 3, 2)





#Fonction phi inverse sur une matrice avec les paramètre a et b
def phi_inverse(mat_orig, a, b):
    mat = np.copy(mat_orig)      #On copie la matrice originale pour la modifier
    (n, p) = mat.shape
    for i in range (0, n):
        for j in range (0, n):
            mat[i,j] = mat_orig[(i*(a*b+1)-j*a)%n, (-i*b+j)%n] #Fonction phi inverse sur chaque élément de la matrice
    return mat

#Fonction de déchiffrement du texte
def dechiffre(fichier_lecture, fichier_ecriture, a, b, n):
    texte_lire = open(fichier_lecture, 'r')
    lect = texte_lire.read()                 #On met le texte dans une chaine de caractères
    longueur = len(lect)
    taille = int(sqrt(longueur))
    A = zeros((taille, taille), dtype=str)   #On créé une matrice de caractères
    A[:] = '0'
    i = 0
    j = 0
    for k in range (0,len(lect)):            #Dans chaque case de la matrice, on y met un caractère de la chaine
        A[i,j] = lect[k]
        j+=1
        if j == taille:
            j = 0
            i+=1
    for k in range (0, n):
        A = phi_inverse(A, a, b)             #On applique n fois la fonction phi inverse sur la matrice
    ecrit=""
    for i in range (taille-1, -1, -1):
        for j in range (0, taille):
            ecrit = ecrit + A[i,j]           #On écrit les éléments de la matrice à la suite dans une chaine des caractères
    while (ecrit[-1] == '0'):
        ecrit = ecrit[:-1]                   #On retire les 0 de fin de chaine qui permettaient de compléter la matrice
    texte_ecrire= open(fichier_ecriture, 'w')
    texte_ecrire.write(ecrit)
    texte_ecrire.close()
    texte_lire.close()



# dechiffre("texte_chiffre.txt", "texte_dechiffre.txt", 5, 3, 2)

def main():
    print("Arguments: nomFichier, mode (m), graine (a), (b) ??, nbDeTours (n)")
    # Recupere les arguments
    if len(sys.argv) < 6:
        print("Erreur: Il faut 6 arguments")
    else:
        i   = sys.argv[1]       # Fichier Input
        m   = sys.argv[2]       # Mode d'utilisation (Chiffrement / Dechiffrement)
        a   = int(sys.argv[3])  # Graine 'a' de chiffrement
        b   = int(sys.argv[4])  # Graine 'b' de chiffrement
        n   = int(sys.argv[5])  # Nombre de transformations

        ext = ".lck"            # Prefixe pour l'extention des fichiers verrouilles

        print(i, m, a, b, n)

        if m == "--lock":
            output = i + ext     # Nom du fichier output Ex fichier.txt.lck
            print("Chiffrement vers '" + output + "'")
            chiffre(i, output, a, b, n)

        elif m == "--unlock":
            if i[-4:] == ext:       # Si fichier est bien au format verrouille
                output = i[:-4]     # Nom du fichier output sans l'extention lock
                print("Dechiffrement vers '" + output + "'")
                dechiffre(i, output, 5, 3, 2)
            else:
                print("Erreur: Dechiffrement format de fichier '" + str(i[-4:]) + "' inconnu. Il faut le format '" + ext + "'")

main()
