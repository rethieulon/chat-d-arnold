from __future__ import print_function
from PIL import Image
from numpy import *
import numpy as np
import sys
import os

#---------------------------------------------------#
#                Taille 200*200                     #
#---------------------------------------------------#

#Applique la fonction phi à l'image
def arnold(im_orig, a, b):
    im = np.copy(im_orig)   #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, n):
            im[i,j] = im_orig[(i+j*a)%n, (i*b+j*(1+a*b))%n] #Fonction phi sur chaque pixel
    return im

def chiffre(image_a_chiffrer, image_chiffree, a, b, n):
    im = Image.open(image_a_chiffrer)
    img = np.asarray(im)
    im_chif = np.copy(img)
    for k in range (0,n):
        im_chif = arnold(im_chif, a, b)
    image = Image.fromarray(im_chif, 'RGB')
    image.save(image_chiffree)

# chiffre("image.jpg", "image_chiffree.jpg", 7, 6, 23)



def arnold_inverse(im_orig, a, b):
    im = np.copy(im_orig)   #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, n):
            im[i,j] = im_orig[(i*(a*b+1)-j*a)%n, (-i*b+j)%n] #Fonction phi sur chaque pixel
    return im

def dechiffre(image_a_dechiffrer, image_dechiffree, a, b, n):
    im = Image.open(image_a_dechiffrer)
    img = np.asarray(im)
    im_chif = np.copy(img)
    for k in range (0,n):
        im_chif = arnold_inverse(im_chif, a, b)
    image = Image.fromarray(im_chif, 'RGB')
    image.save(image_dechiffree)


# dechiffre("image_chiffree.jpg", "image_dechiffree.jpg", 7, 6, 23)




def main():
    print("Arguments: nomFichier, mode (m), graine (a), (b) ??, nbDeTours (n)")
    # Recupere les arguments
    if len(sys.argv) < 6:
        print("Erreur: Il faut 6 arguments")
    else:
        i   = sys.argv[1]       # Fichier Input
        m   = sys.argv[2]       # Mode d'utilisation (Chiffrement / Dechiffrement)
        a   = int(sys.argv[3])  # Valeur de la graine de chiffrement
        b   = int(sys.argv[4])  # B ?????
        n   = int(sys.argv[5])  # Nombre de transformations

        ext = ".lck"            # Prefixe pour l'extention des fichiers verrouilles

        print(i, m, a, b, n)

        if m == "--lock":
            output = i[:-4] + ext + i[-4:]     # Nom du fichier output Ex fichier.txt.lck
            print(output)
            print("Chiffrement vers '" + output + "'")
            chiffre(i, output, a, b, n)

        elif m == "--unlock":
            if i[-8:-4] == ext:       # Si fichier est bien au format verrouille
                output = i[:-8] + i[-4:]     # Nom du fichier output sans l'extention lock
                print("Dechiffrement vers '" + output + "'")
                dechiffre(i, output, 5, 3, 2)
            else:
                print("Erreur: Dechiffrement format de fichier '" + str(i[-8:]) + "' inconnu. Il faut le format '" + ext + ".jpg'")

main()
