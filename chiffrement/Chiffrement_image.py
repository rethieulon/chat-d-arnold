from __future__ import print_function
from PIL import Image
from numpy import *
import numpy as np

#Ce programme permet de chiffrer et de dechiffrer une image
#L'image en parametre doit etre carree


#Applique la fonction phi à l'image
def arnold(im_orig, a, b):  # a, b : parametre de la fonction phi
    im = np.copy(im_orig)   #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, n):
            im[i,j] = im_orig[(i+j*a)%n, (i*b+j*(1+a*b))%n] #Fonction phi sur chaque pixel
    return im

def chiffre(image_a_chiffrer, image_chiffree, a, b, n): # a, b : parametre de la fonction phi
    im = Image.open(image_a_chiffrer)   #Ouvre l'image
    img = np.asarray(im)                #Transforme l'image en matrice
    im_chif = np.copy(img)              #Copie la matrice pour la modifier
    for k in range (0,n):
        im_chif = arnold(im_chif, a, b) #Applique n fois arnold a la matrice
    image = Image.fromarray(im_chif, 'RGB') #Transforme la matrice en image
    image.save(image_chiffree)  #Enregistre l'image

chiffre("image.jpg", "image_chiffree.jpg", 7, 6, 23)



def arnold_inverse(im_orig, a, b):  # a, b : parametre de la fonction phi
    im = np.copy(im_orig)   #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, n):
            im[i,j] = im_orig[(i*(a*b+1)-j*a)%n, (-i*b+j)%n] #Fonction phi sur chaque pixel
    return im

def dechiffre(image_a_dechiffrer, image_dechiffree, a, b, n):   # a, b : parametre de la fonction phi
    im = Image.open(image_a_dechiffrer) #Ouvre l'image
    img = np.asarray(im)                #Transforme l'image en matrice
    im_chif = np.copy(img)              #Copie la matrice pour la modifier
    for k in range (0,n):
        im_chif = arnold_inverse(im_chif, a, b) #Applique n fois arnold inverse a la matrice
    image = Image.fromarray(im_chif, 'RGB')     #Transforme la matrice en image
    image.save(image_dechiffree)    #Enregistre l'image
    

dechiffre("image_chiffree.jpg", "image_dechiffree.jpg", 7, 6, 23)






