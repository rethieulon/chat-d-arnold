from math import *
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numpy import *
import numpy as np

#Ce programme affiche un tore en 3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

theta = np.linspace(0, 2 * np.pi, 20)   #Liste de 20 points entre 0 et 2 pi
phi = np.linspace(0, 2 * np.pi, 20)     #Liste de 20 points entre 0 et 2 pi

X = np.outer((1 + 0.2 * np.cos(phi)), np.cos(theta))        #
Y = np.outer((1 + 0.2 * np.cos(phi)), np.sin(theta))        #Fonction du tore en 3D de petit rayon 0.2 et de grand rayon 1
Z = np.outer(np.sin(phi), 0.2 * np.ones(np.size(theta)))    #

ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet)
ax.set_zlim3d(-0.45,0.45)
plt.show()
