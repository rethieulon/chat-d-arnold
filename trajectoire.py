from numpy import*
import numpy as np
from matplotlib.pyplot import *
import matplotlib.pyplot as plt


def trajectoire(n,i,j): # n : taille matrice ; i, j = position de départ
    Lx=[i]      #Listes des coordonnees des positions
    Ly=[j]
    x, y = (i+j)%n, (i+2*j)%n   #On applique la fonction phi
    while x!=i or y!=j:         #Tant qu'on ne revient pas sur la position du début
        Lx.append(x)            #On ajoute la nouvelle position aux listes
        Ly.append(y)
        x, y = (x+y)%n, (x+2*y)%n   #On applique phi
    Lx.append(x)        #On ajoute la nouvelle position aux listes
    Ly.append(y)
    plt.plot(Lx,Ly,'ro')    #Affiche les differentes positions des listes sur le graphique
    plt.plot([0-5,n+5],[0-2.5,n//2+2.5],'b-',[0-5,n+5],[n//2-2.5,n+2.5],'b-',[0-5,n//4+5],[n//2+10,0-10],'b-',[n//4-5,3*n//4+5],[n+10,0-10],'b-',[3*n//4-5,n+5],[n+10,n//2-10],'b-')    #Affiche les droites qui separent les motifs
    plt.show()      #Affiche le graphique



                        #Division en cinq fois le meme motif
trajectoire(100,47,4)   #Ne marche pas toujours //(100,49,2) par exemple
