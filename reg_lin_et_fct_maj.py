from numpy import *
import numpy as np
from matplotlib.pyplot import *
import matplotlib.pyplot as plt

#On va appeler 'période' le nombre d'itérations de la fonction phi nécessaires pour revenir à létat initial
#Ce programme affiche une droite lineaire majorante des trajectoires de coefficient minimum



#Créé une liste de la période que chaque pixel peut avoir
def liste_periode(n, a):    #Pour une image de taille n
    M = np.zeros([n, n]) #Créé une matrice de zéros
    L = []               #Liste de période des pixels
    N = 1
    while N < n**2:           #Tant que tout les pixels ne sont pas traités
        x, y = case_nulle(M)  #Cherche un pixel qui n'a pas été traité
        T = 1
        M[x,y] = 1            #A chaque pixel traité, on met un 1 à son emplacement
        i, j = (x+y)%n, (a*x+(a+1)*y)%n #Applique la fonction phi sur le pixel
        while  i!=x or j!=y:      #Tant qu'on n'est pas revenu à sa place initiale
            T = T + 1             #Incrémente la période du pixel
            M[i,j] = 1            #A chaque pixel traité, on met un 1 à son emplacement
            i, j = (i+j)%n, (a*i+(a+1)*j)%n #Applique la fonction phi sur le pixel
        L.append(T)    #Ajoute la période à la lite des périodes
        N = N + T      #Ajoute le nombre de pixels traités
    return L


#Cherche un pixel qui n'a pas encore été traité
def case_nulle(M):
    x = 0  #Initialise à 0 l'indice de ligne
    y = 0  #Initialise à 0 l'indice de colonne
    n = len(M[0]) #Cherche la dimension de la matrice
    while M[x,y] == 1: #Tant qu'on est sur un pixel déjà traité
        if y+1 < n:   
            y = y + 1  #Incrémente l'indice de colonne
        else:
            y = 0      #Sinon remet l'indice de colonne à 0
            x = x + 1  #Et incrémente l'indice de ligne
    return (x, y)      #Retourne la position du pixel non traité


#Calcule le PGCD d'un couple
def PGCD(a, b):
    c = max(a, b)
    d = min(a, b)
    while d != 0:
        c, d = d, c%d
    return c


#Calcule le PPCM d'un couple à partir du PGCD
def PPCM(a, b):
    return((a*b)/PGCD(a,b))


#Calcule de PPCM d'une liste
def PPCM_liste(L):
    ppcm = 1
    for elem in L:
        ppcm = PPCM(ppcm, elem)
    return ppcm


#Calcule le PPCM de la liste des périodes des pixels pour donner la période de l'image
def periode(n, a):
    return(PPCM_liste(liste_periode(n, a)))



def reg_lin(n, a): #retourn le coefficient d'une droite linéaire
    x=0    
    y=0    
    for k in range (1, n+1):
        x += k**2          
        y += k*periode(k, a)
    return(y/x)

def fct_maj(n, a):
    m=0
    for k in range (1, n+1):
        m=max(m,periode(k, a)/k)
    return(m)
        
    
print(reg_lin(50,2))
print(fct_maj(50,2))




