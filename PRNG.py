from numpy import *
import numpy as np
from matplotlib.pyplot import *
import matplotlib.pyplot as plt
import operator
import collections
import sys
from Arnold import *

#On va appeler 'période' le nombre d'itérations de la fonction phi nécessaires pour revenir à létat initial
#Ce programme créé un graphique de la période d'une image en fonction de sa dimension


#Créé une liste de la période que chaque pixel peut avoir
def liste_periode(n, a):    #Pour une image de taille n
    M = np.zeros([n, n]) #Créé une matrice de zéros
    L = []               #Liste de période des pixels
    N = 1
    while N < n**2:           #Tant que tout les pixels ne sont pas traités
        x, y = case_nulle(M)  #Cherche un pixel qui n'a pas été traité
        T = 1
        M[x,y] = 1            #A chaque pixel traité, on met un 1 à son emplacement
        i, j = ((x*a)+(y*(a+1)))%n, (((a-1)*x) + (a*y))%n #Applique la fonction phi sur le pixel
        while  i!=x or j!=y:      #Tant qu'on n'est pas revenu à sa place initiale
            T = T + 1             #Incrémente la période du pixel
            M[i,j] = 1            #A chaque pixel traité, on met un 1 à son emplacement
            i, j = ((i*a)+(j*(a+1)))%n, (((a-1)*i) + (a*j))%n #Applique la fonction phi sur le pixel
        L.append(T)    #Ajoute la période à la lite des périodes
        N = N + T      #Ajoute le nombre de pixels traités
    return L

#Cherche un pixel qui n'a pas encore été ttraité
def case_nulle(M):
    x = 0  #Initialise à 0 l'indice de ligne
    y = 0  #Initialise à 0 l'indice de colonne
    n = len(M[0]) #Cherche la dimension de la matrice
    while M[x,y] == 1: #Tant qu'on est sur un pixel déjà traité
        if y+1 < n:
            y = y + 1  #Incrémente l'indice de colonne
        else:
            y = 0      #Sinon remet l'indice de colonne à 0
            x = x + 1  #Et incrémente l'indice de ligne
    return (x, y)      #Retourne la position du pixel non traité


#Calcule le PGCD d'un couple
def PGCD(a, b):
    c = max(a, b)
    d = min(a, b)
    while d != 0:
        c, d = d, c%d
    return c


#Calcule le PPCM d'un couple à partir du PGCD
def PPCM(a, b):
    return((a*b)/PGCD(a,b))


#Calcule de PPCM d'une liste
def PPCM_liste(L):
    ppcm = 1
    for elem in L:
        ppcm = PPCM(ppcm, elem)
    return ppcm


#Calcule le PPCM de la liste des périodes des pixels pour donner la période de l'image
def periode(n, a):
    return(PPCM_liste(liste_periode(n, a)))



def fct_maj(n, a):
    m=0
    for k in range (1, n+1):
        m=max(m,periode(k, a)/k)
    return(m)


def contingence_periodes(L):
    C          = dict() # Dictionnaire de contingence
    C_sort     = dict() # Dictionnaire trié (sur la valeur de la periode pour l'instant)
    valeurs    = []     # Liste des valeurs trouvees
    occurences = []     # Nombre d'occurences de chaque valeur

    for i in L:
        if i in C:
            C[i] += 1
        else:
            C.update({i: 1}) # Creation d'une nouvelle entree dans le dico avec la clé de valeur L[i] et de valeur associée 1

    C_sort = sorted(C.items(), key=operator.itemgetter(0)) # Tri par ordre croissant sur la valeur

    # Separation de la contingence en 2 listes
    for item in C_sort:
        valeurs.append(item[0])
        occurences.append(item[1])

    return valeurs, occurences


def afficher_contingence(valeurs, occurences):
    plt.rcParams['figure.figsize'] = [15, 5] # L'image fera 15 pouces de large par 5 de haut
    y_pos = np.arange(len(occurences))       # Pour faire une liste à partir des index
    plt.bar(y_pos, occurences)
    plt.xticks(y_pos, valeurs)
    plt.show()

def afficher_repartition(n,m,a):
    Lx=[]
    for k in range (int(sqrt(m)),n+1): # Tronquage des valeurs inférieures à la racine de la borne max
        Lx.append(k)
    Ly=liste_random(n,m,a)

    plt.plot(Lx, Ly, 'ro')
    coef = fct_maj(n,a)
    plt.plot([0,n], [0,coef])  #Affiche la droite majorante

    plt.show()

# n: nombre de periodes
# m: borne maximale de notre générateur
# a: graine pour générer la matrice de transformation
def liste_random(n, m, a):
    L=[]
    coef=fct_maj(n,a)
    for k in range (int(sqrt(m)), n+1):
        p   = periode(k, a)
        ord = (p * k) % m
        L.append(int(ord) + 1)
    return(L)

def creer_liste(n,m,a):
    Lx = range(1, n)
    Ly = []
    #for k in range (int(sqrt(m)),n+1): # Tronquage des valeurs inférieures à la racine de la borne max
        #Lx.append(k)
    Ly = liste_random(n,m,a)

    return Lx, Ly


# TEST POUR LE PRNG
def main():
    print("Arguments: nb periodes (n), valeur max (m), graine (a), nom fichier de sortie ")
    # Recupere les arguments
    n   = int(sys.argv[1])
    m   = int(sys.argv[2])
    a   = int(sys.argv[3])
    o   = sys.argv[4]      # nom du fichier output

    print(n, m, a, o)

    # Execute la recherche des periodes
    Ly = liste_random(n, m, a)

    # Ecrit dans un fichier
    f = open(o, "w")
    f.write(str(Ly).strip('[').replace(',', '').replace(']','\r\n') )
    f.close()


### MAIN ###
toto()
# main()
