from numpy import *
import numpy as np
from matplotlib.pyplot import *
import matplotlib.pyplot as plt

#On va appeler 'période' le nombre d'itérations de la fonction phi nécessaires pour revenir à létat initial


#Créé une liste de la période que chaque pixel peut avoir
def liste_periode(n, a):    #Pour une image de taille n
    M = np.zeros([n, n]) #Créé une matrice de zéros
    L = []               #Liste de période des pixels
    N = 1
    while N < n**2:           #Tant que tout les pixels ne sont pas traités
        x, y = case_nulle(M)  #Cherche un pixel qui n'a pas été traité
        T = 1
        M[x,y] = 1            #A chaque pixel traité, on met un 1 à son emplacement
        i, j = (x+y)%n, (a*x+(a+1)*y)%n #Applique la fonction phi sur le pixel
        while  i!=x or j!=y:      #Tant qu'on n'est pas revenu à sa place initiale
            T = T + 1             #Incrémente la période du pixel
            M[i,j] = 1            #A chaque pixel traité, on met un 1 à son emplacement
            i, j = (i+j)%n, (a*i+(a+1)*j)%n #Applique la fonction phi sur le pixel
        L.append(T)    #Ajoute la période à la lite des périodes
        N = N + T      #Ajoute le nombre de pixels traités
    return L


#Cherche un pixel qui n'a pas encore été traité
def case_nulle(M):
    x = 0  #Initialise à 0 l'indice de ligne
    y = 0  #Initialise à 0 l'indice de colonne
    n = len(M[0]) #Cherche la dimension de la matrice
    while M[x,y] == 1: #Tant qu'on est sur un pixel déjà traité
        if y+1 < n:   
            y = y + 1  #Incrémente l'indice de colonne
        else:
            y = 0      #Sinon remet l'indice de colonne à 0
            x = x + 1  #Et incrémente l'indice de ligne
    return (x, y)      #Retourne la position du pixel non traité


#Calcule le PGCD d'un couple
def PGCD(a, b):
    c = max(a, b)
    d = min(a, b)
    while d != 0:
        c, d = d, c%d
    return c


#Calcule le PPCM d'un couple à partir du PGCD
def PPCM(a, b):
    return((a*b)/PGCD(a,b))


#Calcule de PPCM d'une liste
def PPCM_liste(L):
    ppcm = 1
    for elem in L:
        ppcm = PPCM(ppcm, elem)
    return ppcm


#Calcule le PPCM de la liste des périodes des pixels pour donner la période de l'image
def periode(n, a):
    return(PPCM_liste(liste_periode(n, a)))


#Cherche le coefficient minimum de la droite lineaire majorent toutes les periodes
def fct_maj(n, a):
    m=0
    for k in range (1, n+1):
        m=max(m,periode(k, a)/k)
    return(m)


def liste_random(n, m, a):# n : nb de nombres souhaités, k : borne sup, a : graine
    L=[]
    coef=fct_maj(n,a)   #Droite lineaire majorante
    for k in range (1, n+1):
        L.append(int(m*periode(k, a)/(k*coef))) #Ajoute a la liste le nombre pseudo-aleatoire (rapport d'une periode par la droite majorante en abscisse de la taille de l'image)
    return(L)

#Affiche la repartition des nombres pseudo-aleatoires (pour observer l'equirepartition)
def affiche_repartition(n,m,a):
    Lx=[]
    for k in range (1,n+1):
        Lx.append(k)
    Ly=liste_random(n,m,a)
    plt.plot(Lx, Ly, 'ro')
    plt.show()
        


print(affiche_repartition(50,100,1))












