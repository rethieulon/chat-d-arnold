from __future__ import print_function
from PIL import Image
from numpy import *
import numpy as np
from cv2 import VideoWriter, VideoWriter_fourcc



#Ouvre l'image
im = Image.open("Capture2.JPG")

#Découpe une partie de l'image
couper = (0, 0, 200, 200)
imcut = im.crop(couper)

#Transforme l'image en matrice
img = np.asarray(imcut)


#Applique la fonction phi à l'image
def arnold(im_orig):
    im = np.copy(im_orig)   #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, n):
            im[i,j] = im_orig[(i+j)%n, (i+2*j)%n] #Fonction phi sur chaque pixel
    return im


#Echange la couleur bleue et la couleur rouge car la fonction de création de vidéo ne prend pas les mêmes paramètres
def inverse_couleur(img):
    im = img.copy()         #Copie l'image originale pour la modifier
    (n, p, q) = im.shape    #Dimensions de l'image
    for i in range (0, n):
        for j in range (0, p):
            im[i,j,0], im[i,j,2] = im[i,j,2], im[i,j,0] #Echange la couleur de chaque pixel
    return im




taille = (200, 200)
FPS = 30
seconds = 10

fourcc = VideoWriter_fourcc(*'MP42')
video = VideoWriter('./arnold.avi', fourcc, float(FPS), taille)


#Créé la vidéo de plusieurs itérations de la fonction arnold
def creer_video(im, FPS, seconds):
    img = inverse_couleur(im) #Echange les couleurs
    video.write(img)          #Ajoute l'image originale au début de la vidéo
    for _ in range (0, FPS*seconds):
        img = arnold(img)
        video.write(img)      #Ajoute chaque itération de la fonction arnold à la vidéo
    video.release()

def toto():
    print("Hello world")

# creer_video(img, FPS, seconds)
